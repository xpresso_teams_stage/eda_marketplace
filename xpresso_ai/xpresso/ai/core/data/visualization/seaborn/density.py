import pandas as pd
import seaborn as sns

import xpresso.ai.core.data.visualization.utils as utils
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InputLengthMismatch
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot


class NewPlot(Plot):
    """
    Generates a density plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): data for plotting
        target(list): target variable data
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """

    def __init__(self, input_1, target=None, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH):
        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels

        if target is not None:
            if len(input_1) != len(target):
                raise InputLengthMismatch
            data = pd.DataFrame(list(zip(input_1, target)),
                                columns=["Input", "Target"])
            category_list = data.Target.unique()
            DataFrameDict = {category: pd.DataFrame for category in
                             category_list}
            for key in DataFrameDict.keys():
                DataFrameDict[key] = data["Input"][
                    data.Target == key].dropna().tolist()
                self.plot = sns.kdeplot(data=DataFrameDict[key], shade=True)
            self.plot.legend(labels=DataFrameDict.keys()).set_title(
                self.axes_labels[utils.TARGET_LABEL])
        else:
            self.plot = sns.kdeplot(data=input_1, shade=True)
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL],
                      title=plot_title)
        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)
